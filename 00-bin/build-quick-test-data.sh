#! /bin/bash

SCRIPTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
PYTHON="python3"

TIMESTAMP=$(date +%Y-%m-%d_%H-%M-%S)
RESDIR="$SCRIPTDIR/../var/$TIMESTAMP"
SIMDATADIR="$RESDIR/01-inphest-generated"
CLASSIFIERDIR="$RESDIR/02-classification"

if [[ -z $1 ]]
then
    NREPS_TRAINING=100
else
    NREPS_TRAINING=$1
fi

if [[ -z $2 ]]
then
    NREPS_TESTING=10
else
    NREPS_TESTING=$2
fi

echo "Training data: $NREPS_TRAINING replicates"
echo "Test data: $NREPS_TESTING replicates"

set -e -o pipefail
set -x

mkdir -p $SIMDATADIR
mkdir -p $CLASSIFIERDIR

# Simulation

## simulate unconstrained training data
$PYTHON $SCRIPTDIR/simulate.py \
        --nreps $NREPS_TRAINING \
        -o $SIMDATADIR/target.unconstrained  \
        --debug \
        -F archipelago \
        -H 01-host-input-data/H001.no_area_loss.reps1.histories.json \
        unconstrained

## simulate constrained training data
$PYTHON $SCRIPTDIR/simulate.py \
        --nreps $NREPS_TRAINING  \
        -o $SIMDATADIR/target.constrained \
        --debug \
        -F archipelago \
        -H 01-host-input-data/H001.no_area_loss.reps1.histories.json \
        constrained

## simulate testing target data (unconstrained cases to be classified)
$PYTHON $SCRIPTDIR/simulate.py \
        --nreps $NREPS_TESTING \
        -o $SIMDATADIR/training.unconstrained \
        --debug -F archipelago \
        -H 01-host-input-data/H001.no_area_loss.reps1.histories.json \
        unconstrained

## simulate testing target data (constrained cases to be classified)
$PYTHON $SCRIPTDIR/simulate.py \
        --nreps $NREPS_TESTING \
        -o $SIMDATADIR/training.constrained \
        --debug -F archipelago \
        -H 01-host-input-data/H001.no_area_loss.reps1.histories.json \
        constrained &&

# Classification

archipelago-classify.py \
        -o $CLASSIFIERDIR/results.performance.unconstrained.csv \
        --describe $CLASSIFIERDIR/results.unconstrained.txt \
        --opt 0 \
        --true-model unconstrained \
        $SIMDATADIR/target.unconstrained.summary-stats.csv \
        $SIMDATADIR/training.unconstrained.summary-stats.csv \
        $SIMDATADIR/training.constrained.summary-stats.csv > $RESDIR/results.constrained.csv

archipelago-classify.py \
        -o $CLASSIFIERDIR/results.performance.constrained.csv   \
        --describe $CLASSIFIERDIR/results.constrained.txt   \
        --opt 0 --true-model constrained   \
        $SIMDATADIR/target.constrained.summary-stats.csv \
        $SIMDATADIR/training.unconstrained.summary-stats.csv \
        $SIMDATADIR/training.constrained.summary-stats.csv > $RESDIR/results.unconstrained.csv

