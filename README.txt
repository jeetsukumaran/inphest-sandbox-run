# Install


## For Simulator

~~~
$ python -m pip install git+https://github.com/jeetsukumaran/DendroPy.git@development-master
$ python -m pip install git+https://github.com/jeetsukumaran/inphest.git
~~~


## For Classifier


### Archipelago

~~~
$ python -m pip install git+https://github.com/jeetsukumaran/archipelago.git@development-master
~~~

### R + adegenet

~~~
> install.packages("adegenet")
~~~


# Run

~~~
$ 00-bin/build-quick-test-data.sh
~~~
